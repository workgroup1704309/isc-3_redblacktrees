package usalesiana.example.redBlackTree;

public interface IRedBlackNode<T> {
    T getData();
    void setData(T data);

    Color getColor();
    void setColor(Color color);

    RedBlackNode<T> getParentNode();
    void setParentNode(RedBlackNode<T> parent);

    RedBlackNode<T> getLeftNode();
    void setLeftNode(RedBlackNode<T> leftNode);

    RedBlackNode<T> getRightNode();
    void setRightNode(RedBlackNode<T> rightNode);
}
