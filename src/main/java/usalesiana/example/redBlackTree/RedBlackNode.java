package usalesiana.example.redBlackTree;

public class RedBlackNode<T> implements IRedBlackNode<T> {
    private T data;
    private RedBlackNode<T> parentNode;
    private RedBlackNode<T> leftNode;
    private RedBlackNode<T> rightNode;
    private Color color;

    public RedBlackNode(T data, Color color) {
        this.data = data;
        this.color = color;

        this.parentNode = null;
        this.leftNode = null;
        this.rightNode = null;
    }

    @Override
    public T getData() {
        return this.data;
    }

    @Override
    public void setData(T data) {
        this.data = data;
    }

    @Override
    public Color getColor() {
        return this.color;
    }

    @Override
    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public RedBlackNode<T> getParentNode() {
        return this.parentNode;
    }

    @Override
    public void setParentNode(RedBlackNode<T> parent) {
        this.parentNode = parent;
    }

    @Override
    public RedBlackNode<T> getLeftNode() {
        return this.leftNode;
    }

    @Override
    public void setLeftNode(RedBlackNode<T> leftNode) {
        this.leftNode = leftNode;
    }

    @Override
    public RedBlackNode<T> getRightNode() {
        return this.rightNode;
    }

    @Override
    public void setRightNode(RedBlackNode<T> rightNode) {
        this.rightNode = rightNode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RedBlackNode<?> node = (RedBlackNode<?>) o;
        return node.getData().equals(data);
    }
}
