package usalesiana.example.redBlackTree;

public interface IRedBlackTree<T> {
    RedBlackNode<T> getRootNode();
    void setRootNode(RedBlackNode<T> rootNode);

    RedBlackNode<T> getNilNode();
    void setNilNode(RedBlackNode<T> node);

    void insert(T data);
    void insert(T data, RedBlackNode<T> parentNode);

    void remove(T data);

    RedBlackNode<T> find(T data);
    boolean contains(T data);

    void printTree();

    void rotateLeft(RedBlackNode<T> node);
    void rotateRight(RedBlackNode<T> node);

    void printIterativeBFS();
    void printRecursiveBFS();

    int getHeight();
    int getWeight();

    // DFS algorithm
    void traversalPreOrderIterativeDFS();
    void traversalPreOrderRecursiveDFS(RedBlackNode<T> node);

    void traversalInOrderIterativeDFS();
    void traversalInOrderRecursiveDFS(RedBlackNode<T> node);

    void traversalPostOrderIterativeDFS();
    void traversalPostOrderRecursiveDFS(RedBlackNode<T> node);
}
