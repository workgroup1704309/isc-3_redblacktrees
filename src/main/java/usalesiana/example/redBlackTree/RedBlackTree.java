package usalesiana.example.redBlackTree;

import static usalesiana.example.redBlackTree.Color.BLACK;
import static usalesiana.example.redBlackTree.Color.RED;

public class RedBlackTree<T extends  Comparable<T>> implements IRedBlackTree<T> {
    private RedBlackNode<T> root;
    private RedBlackNode<T> nil;
    private int height;
    private boolean processedHeight;

    @Override
    public RedBlackNode<T> getRootNode() {
        return  root;
    }

    @Override
    public void setRootNode(RedBlackNode<T> rootNode) {
        this.root = rootNode;
    }

    @Override
    public RedBlackNode<T> getNilNode() {
        return nil;
    }

    @Override
    public void setNilNode(RedBlackNode<T> node) {
        this.nil = node;
    }

    @Override
    public void insert(T data) {//16
        // First check if node exists
        if (this.find(data) == null) {
            //Step 1
            //New node -> data = 1
            //Color -> Black by default
            var newNode = new RedBlackNode<T>(data, BLACK);//16 - C: B
            RedBlackNode<T> node = null;
            //Step 2
            //Verify if the tree is empty (ie. whether root node is NIL).
            // If yes, insert newNode as a root node and color black.
            if (this.root == null) {
                this.setRootNode(newNode);
            } else {
                var rootNodeCopy = this.root;
                // Step 3
                // Traverse the tree to the left or right depending on the data
                while (rootNodeCopy != this.nil) {
                    node = rootNodeCopy;
                    if (newNode.getData().compareTo(rootNodeCopy.getData()) < 0) {
                        rootNodeCopy = rootNodeCopy.getLeftNode();
                    } else {
                        rootNodeCopy = rootNodeCopy.getRightNode();
                    }
                }
            }
            // Step 4
            // Assign the parent of the leaf as parent of newNode.
            newNode.setParentNode(node);
            if (node != null) {
                // Step 5
                // if leafData is less than newData, make newNode as leftChild (*)
                if (newNode.getData().compareTo(node.getData()) < 0) {
                    node.setLeftNode(newNode);
                } else {
                    //Step 6
                    //Otherwise (else), make newNode as rightChild
                    node.setRightNode(newNode);
                }
            }

            //Step 7
            // Assign NIL to the leftChild and rightChild of newNode
            newNode.setRightNode(null);
            newNode.setLeftNode(null);
            //Step 8
            // Assign RED color to newNode (Revalidate Props)
            newNode.setColor(RED);
            //Step 9
            // revalidate tree (operation)
            // revalidateTree(newNode);
            revalidateAfterInsert(newNode);

        } else {
            System.out.println("This value: " + data + " already exists!");
        }
    }

    private void revalidateAfterInsert(RedBlackNode<T> node) {// 8 C:R  Parent -> 9 C:R
        RedBlackNode<T> parent = node.getParentNode(); // 9

        // Case 1: Parent is null, we've reached the root, the end of the recursion
        if (parent == null) {
            // node.setColor(BLACK);
            return;
        }

        // Parent is black?? --> nothing to do
        if (parent.getColor() == BLACK) {
            return;
        }

        // From here on, parent is red
        RedBlackNode<T> grandparent = parent.getParentNode(); // 9 -> P=7

        // Case 2:
        // Not having a grandparent means that parent is the root. If we enforce black roots
        // (rule 2), grandparent will never be null, and the following if-then block can be
        // removed.
        if (grandparent == null) {
            parent.setColor(BLACK);
            return;
        }

        // Get the uncle (may be null/nil, in which case its color is BLACK)
        RedBlackNode<T> uncle = this.getUncle(parent); // 16 C-R

        // Case 3: Uncle is red -> recolor parent, grandparent and uncle
        if (uncle != null && uncle.getColor() == RED) {
            parent.setColor(BLACK);
            grandparent.setColor(RED);
            uncle.setColor(BLACK);

            // Call recursively for grandparent, which is now red.
            // It might be root or have a red parent, in which case we need to fix more...
            revalidateAfterInsert(grandparent);
        }

        // Parent is left child of grandparent
        else if (parent == grandparent.getLeftNode()) {
            // Case 4a: Uncle is black and node is left->right "inner child" of its grandparent
            if (node == parent.getRightNode()) {
                rotateLeft(parent);

                // Let "parent" point to the new root node of the rotated sub-tree.
                // It will be recolored in the next step, which we're going to fall-through to.
                parent = node;
            }

            // Case 5a: Uncle is black and node is left->left "outer child" of its grandparent
            rotateRight(grandparent);

            // Recolor original parent and grandparent
            parent.setColor(BLACK);
            grandparent.setColor(RED);
        }

        // Parent is right child of grandparent
        else {
            // Case 4b: Uncle is black and node is right->left "inner child" of its grandparent
            if (node == parent.getLeftNode()) {
                rotateRight(parent);

                // Let "parent" point to the new root node of the rotated sub-tree.
                // It will be recolored in the next step, which we're going to fall-through to.
                parent = node;
            }

            // Case 5b: Uncle is black and node is right->right "outer child" of its grandparent
            rotateLeft(grandparent);

            // Recolor original parent and grandparent
            parent.setColor(BLACK);
            grandparent.setColor(RED);
        }
    }

    private RedBlackNode<T> getUncle(RedBlackNode<T> parent) { // 7 -> 10
        RedBlackNode<T> grandparent = parent.getParentNode();//10
        
        if (grandparent.getLeftNode() == parent) { // 7 == 7
            return grandparent.getRightNode(); // -> uncle = 16
        } else if (grandparent.getRightNode() == parent) {
            return grandparent.getLeftNode();
        }

        return null;
    }

    private void revalidateTree(RedBlackNode<T> node){
        RedBlackNode<T> uncle;
        boolean isRightParent;
        while (node != null && node.getParentNode() != null && node.getParentNode().getColor().equals(RED)) {
            isRightParent = node.getParentNode().equals(node.getParentNode().getParentNode().getRightNode());
            uncle = isRightParent ? node.getParentNode().getParentNode().getLeftNode() : node.getParentNode().getParentNode().getRightNode() ;
            if (uncle != null && uncle.getColor().equals(RED)) {
                uncle.setColor(BLACK);
                node.getParentNode().setColor(BLACK);
                node.getParentNode().getParentNode().setColor(RED);
                node = node.getParentNode().getParentNode();
            } else {
                if ((isRightParent && node.equals(node.getParentNode().getLeftNode())) ||(!isRightParent && node.equals(node.getParentNode().getRightNode()))) {
                    node = node.getParentNode();
                    if(node != null){
                        if(isRightParent) rotateRight(node);else rotateLeft(node);
                    }
                }
                assert node != null;
                node.getParentNode().setColor(BLACK);
                node.getParentNode().getParentNode().setColor(RED);
                if(isRightParent) rotateLeft(node.getParentNode().getParentNode());else rotateRight(node.getParentNode().getParentNode());
            }

            if (node.equals(this.root)) {
                break;
            }
        }

        this.root.setColor(BLACK);
    }

    @Override
    public void insert(T data, RedBlackNode<T> parentNode) {

    }

    @Override
    public void remove(T data) {

    }

    @Override
    public RedBlackNode<T> find(T data) {
        return findRecursive(root, data);
    }

    private RedBlackNode<T> findRecursive(RedBlackNode<T> root, T data) {
        if (root == null  || root.getData().compareTo(data) == 0) {
            return root;
        }
        if (data.compareTo(root.getData()) < 0) {
            return findRecursive(root.getLeftNode(), data);
        }

        return findRecursive(root.getRightNode(), data);
    }

    @Override
    public boolean contains(T data) {
        return false;
    }

    @Override
    public void printTree() {
        printTreeRecursive(this.root, "", true);
    }
    private void printTreeRecursive(RedBlackNode<T> node, String separator, boolean isLast ) {
        if (node != this.nil) {
            System.out.print(separator);
            if (isLast) {
                System.out.print("ROOT----");
                separator += "   ";
            } else {
                System.out.print("LEVEL----");
                separator += "|  ";
            }

            System.out.println(node.getData() + "(" + node.getColor() + ")");

            if (node.getLeftNode() != null) {
                printTreeRecursive(node.getLeftNode(), separator, false);
            }

            if (node.getRightNode() != null) {
                printTreeRecursive(node.getRightNode(), separator, false);
            }
        }
    }

    @Override
    public void rotateLeft(RedBlackNode<T> node) {
        RedBlackNode<T> actualLeft = node.getLeftNode();
        node.setLeftNode(actualLeft.getRightNode());
        if (node.getLeftNode() != null) {
            node.getLeftNode().setParentNode(node);
        }
        actualLeft.setRightNode(node);
        actualLeft.setParentNode(node.getParentNode());
        updateOverParentChildren(node, actualLeft);
        node.setParentNode(actualLeft);
    }
    private void updateOverParentChildren(RedBlackNode<T> actNode, RedBlackNode<T> repNode) {
        if (actNode.getParentNode() == null) {
            root = repNode;
        } else {
            if (actNode.equals(actNode.getParentNode().getLeftNode())) {
                actNode.getParentNode().setLeftNode(repNode);
            }else{
                actNode.getParentNode().setRightNode(repNode);
            }
        }
    }

    @Override
    public void rotateRight(RedBlackNode<T> node) {
        RedBlackNode<T> actualRight = node.getRightNode();
        node.setRightNode(actualRight.getLeftNode());
        if (node.getRightNode() != null) {
            node.getRightNode().setParentNode(node);
        }
        actualRight.setLeftNode(node);
        actualRight.setParentNode(node.getParentNode());
        updateOverParentChildren(node, actualRight);
        node.setParentNode(actualRight);
    }

    @Override
    public void printIterativeBFS() {

    }

    @Override
    public void printRecursiveBFS() {

    }

    @Override
    public int getHeight() {
        return 0;
    }

    @Override
    public int getWeight() {
        return 0;
    }

    @Override
    public void traversalPreOrderIterativeDFS() {

    }

    @Override
    public void traversalPreOrderRecursiveDFS(RedBlackNode<T> node) {

    }

    @Override
    public void traversalInOrderIterativeDFS() {

    }

    @Override
    public void traversalInOrderRecursiveDFS(RedBlackNode<T> node) {

    }

    @Override
    public void traversalPostOrderIterativeDFS() {

    }

    @Override
    public void traversalPostOrderRecursiveDFS(RedBlackNode<T> node) {

    }
}
