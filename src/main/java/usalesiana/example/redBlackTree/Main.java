package usalesiana.example.redBlackTree;

public class Main {
    public static void main(String[] args) {
//        System.out.println("Hello World!");

        IRedBlackTree<Integer> tree = new RedBlackTree<>();
        // B-10
        // L-7(BLACK)  R-16(RED)
        // L-15(BLACK) R-18(BLACK)
        // R-30(RED)

        // 3 inserted
        // B-10
        // L-7(BLACK)  R-16(RED)
        // L-3(RED)
        // L-15(BLACK) R-18(BLACK)
        // R-30(RED)

        // B-10
        // L-7(RED)  R-16(RED)
        // L-3(BLACK)  R-9(BLACK)
        // L-8(BLACK)
        // L-15(BLACK) R-18(BLACK)
        // R-30(RED)

        tree.insert(10);
        tree.insert(16);
        tree.insert(7);
        tree.insert(15);
        tree.insert(18);
        tree.insert(30);

        tree.printTree();
        tree.insert(3);
        tree.insert(9);
        tree.insert(8); // LEVEL 3 -> implica una revalidacion

        tree.printTree();
//        System.out.println();
//        System.out.println("16 elimination");
//        tree.remove(16);
//        tree.printTree();
    }
}
